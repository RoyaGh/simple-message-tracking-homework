import org.graalvm.compiler.graph.Graph

object MainClass extends App {

  CommonData.generateMessageLog()

  val messageList = CommonData.messageLog
  println(messageList)

  println(s"Enter a Message ID between 1 and ${MessageIdClass.getLastId}")
  val msgInput = scala.io.StdIn.readInt()

  println("-----------------------------------------------------------------------------")

  //Generate the message log
  var messageGraph = scala.collection.mutable.Map[Int, scala.collection.mutable.Set[Int]]()

  for (i <- messageList) {
    if (i.getMessageId == msgInput &&
      (
        (messageGraph.contains(i.getFromUser) &&
          !messageGraph(i.getFromUser).contains(i.getToUser)) ||
          !messageGraph.contains(i.getFromUser))) {
      println(i.getFromUser + "->" + i.getToUser)
      var tmp: scala.collection.mutable.Set[Int] = messageGraph.getOrElse(i.getFromUser, scala.collection.mutable.Set.empty)
      tmp += i.getToUser
      messageGraph.addOne(i.getFromUser, tmp)
    }
  }

  println("-----------------------------------------------------------------------------")

  println("-----------------------------------------------------------------------------")

  traverseGraph(messageGraph, 0)

  def traverseGraph(graph: scala.collection.mutable.Map[Int, scala.collection.mutable.Set[Int]], head: Int): Unit = {
    println()
    print(s"$head -> ")
    val rec = graph.getOrElse(head, scala.collection.mutable.Set.empty)
    for (rec3 <- rec)
      println(rec3)
    for (rec2 <- rec) {
      graph(head).remove(rec2)
      traverseGraph(graph, rec2)
    }

  }
}
