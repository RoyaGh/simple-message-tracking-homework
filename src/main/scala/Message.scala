class Message (fromUser: Int, toUser: Int, messageId: Int) {

  override def toString: String = s"$messageId : $fromUser -> $toUser"

  def getMessageId: Int = messageId
  def getToUser = toUser
  def getFromUser = fromUser

}
