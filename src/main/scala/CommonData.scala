import scala.collection.mutable.ListBuffer

object CommonData {

  var userList: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
  var messageList = new ListBuffer[Int]();
  var userMessageList = scala.collection.mutable.Map[Int, scala.collection.mutable.ListBuffer[Int]]()
  var messageLog = scala.collection.mutable.ListBuffer[Message]()

  for (i <- userList) userMessageList.addOne(i, ListBuffer.empty)

  def generateMessageLog(): Unit = {
    for (iter <- 1 to 100) {

      //Generate random sender and receiver
      val r = new scala.util.Random
      val sendUser = userList(r.nextInt(userList.length))
      val receiveUser = userList(r.nextInt(userList.length))

      //with probability 1/6 generate a new message to send
      //with probability 5/6 send a message that sender already has.
      val msgId = {
        if (userMessageList(sendUser).length == 0 || r.nextInt(6) == 3) {
          val x = MessageIdClass.generateNextMessageId
          messageList += x
          val m = new Message(0, sendUser, x)
          messageLog.addOne(m)
          x
        }
        else userMessageList(sendUser)(r.nextInt(userMessageList(sendUser).length))
      }
      var message = new Message(sendUser, receiveUser, msgId)

      if (!userMessageList(sendUser).contains(msgId))
        userMessageList(sendUser) += msgId
      if (!userMessageList(receiveUser).contains(msgId))
        userMessageList(receiveUser) += msgId

      messageLog.addOne(message)
    }
  }

  def getMessageLog = messageLog
}
